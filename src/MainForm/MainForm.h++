#pragma once

#include <QtWidgets>


/*!
 * \class MainForm
 * \brief Главное окно программы.
 */
//==============================================================================
class MainForm : public QWidget
{
public:
	MainForm(QWidget *parent = 0);

protected:
	virtual void mousePressEvent(QMouseEvent *e);
	virtual void mouseMoveEvent(QMouseEvent *e);
	virtual void mouseReleaseEvent(QMouseEvent *e);

private:
    QPoint mNewFormPosition;
    bool mIsPressed;
};

QT += gui widgets

CONFIG += c++14 -Wno-c++11-extensions

RESOURCES += \
    res.qrc

SOURCES += \
    src/BurgerCurtain/BurgerBack.c++ \
    src/BurgerCurtain/BurgerCurtain.c++ \
    src/Editor/Widgets/WallWidget.c++ \
    src/Editor/Widgets/WallWidgetDate.c++ \
    src/Editor/Widgets/WallWidgetQuote.c++ \
    src/Editor/Widgets/WallWidgetWeather.c++ \
    src/Editor/Editor.c++ \
    src/Editor/WallAnimateMove.c++ \
    src/Editor/WallGrid.c++ \
    src/MainForm/ChangedStatus.c++ \
    src/MainForm/CloseButton.c++ \
    src/MainForm/IconButton.c++ \
    src/MainForm/MainForm.c++ \
    src/MainForm/MinimizeButton.c++ \
    src/MainForm/NotificationStatus.c++ \
    src/MainForm/WallpaperButton.c++ \
    src/MainForm/WallpaperStatus.c++ \
    src/MainForm/WidgetButton.c++ \
    src/main.c++ \
    src/MainForm/BurgerButton.c++

HEADERS += \
    src/BurgerCurtain/BurgerBack.h++ \
    src/BurgerCurtain/BurgerCurtain.h++ \
    src/Editor/Widgets/WallWidget.h++ \
    src/Editor/Widgets/WallWidgetDate.h++ \
    src/Editor/Widgets/WallWidgetQuote.h++ \
    src/Editor/Widgets/WallWidgetWeather.h++ \
    src/Editor/Editor.h++ \
    src/Editor/WallAnimateMove.h++ \
    src/Editor/WallGrid.h++ \
    src/MainForm/BurgerButton.h++ \
    src/MainForm/ChangedStatus.h++ \
    src/MainForm/CloseButton.h++ \
    src/MainForm/IconButton.h++ \
    src/MainForm/MainForm.h++ \
    src/MainForm/MinimizeButton.h++ \
    src/MainForm/NotificationStatus.h++ \
    src/MainForm/WallpaperButton.h++ \
    src/MainForm/WallpaperStatus.h++ \
    src/MainForm/WidgetButton.h++

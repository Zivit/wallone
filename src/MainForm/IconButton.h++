#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class IconButton
 * \brief Кнопка с иконкой.
 */
//==============================================================================
class IconButton : public QPushButton
{
    Q_OBJECT
public:
    explicit IconButton(QWidget *parent = 0);

    void setIcon(const QString& fileName);
    void setTextColor(const QString& color);
    void setText(const QString& text);

private:
    QLabel* mLabel;

    QString mIconFileName;
    QString mIconFileNameHover;
    QString mTextColor;
    QString mTextColorHover;

    void mUpdate();

signals:

public slots:
};

}

#pragma once

#include <QtWidgets>


/*!
 * \class WallGrid
 * \brief "Шторка" меню.
 */
//==============================================================================
class WallGrid
{
public:
	WallGrid() = default;
	WallGrid(QPoint pos, int col = 1, int row = 1, int w = 70, int h = 70);

	// Gets
    QPoint position() const;
    int columns() const;
    int rows() const;
    int width() const;
    int height() const;

    int allWidth() const;
    int allHeight() const;

	// Methods
    bool isContains(QPoint position, QRect rect);
    QPoint getAlignPoint(QPoint position);

private:
    QPoint mPosition;
    int mColumns;
    int mRows;
    int mWidth;
    int mHeight;

    int mAllWidth;
    int mAllHeight;

    QVector<QRect> mGridRects;
};

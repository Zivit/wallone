#include <QtWidgets>

#include "MainForm/MainForm.h++"


//------------------------------------------------------------------------------
int main(int argc, char** argv)
{
	QApplication app(argc, argv);

	// Main Window
	MainForm mainForm;

	mainForm.show();
	return app.exec();
}

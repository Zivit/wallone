#include "MainForm.h++"

#include "BurgerButton.h++"
#include "MinimizeButton.h++"
#include "CloseButton.h++"
#include "WallpaperButton.h++"
#include "WidgetButton.h++"
#include "IconButton.h++"

#include "NotificationStatus.h++"
#include "ChangedStatus.h++"
#include "WallpaperStatus.h++"

#include "src/BurgerCurtain/BurgerCurtain.h++"
#include "src/Editor/Editor.h++"


//------------------------------------------------------------------------------
MainForm::MainForm(QWidget *parent)
	: QWidget(parent,  Qt::FramelessWindowHint | Qt::Window)
{
	// MainForm background
	this->setGeometry(0, 0, 600, 338);

	QPalette mainPal;
	mainPal.setColor(this->backgroundRole(), QColor("#2e3640"));

	this->setPalette(mainPal);


	// MainForm image background
    auto backImageOpacity = new QGraphicsOpacityEffect;
	backImageOpacity->setOpacity(0.06);

    auto backImage = new QLabel(this);
    backImage->setPixmap(QPixmap(":/res/Images/bg.jpg").scaled(600,
                                                           338,
                                                           Qt::IgnoreAspectRatio));
	backImage->setGraphicsEffect(backImageOpacity);


	// Logo
    auto logoImage = new QLabel(this);
    logoImage->setPixmap(QPixmap(":/res/Images/logo.png"));
	logoImage->move(246, 25);


	// Buttons
    auto burger = new wall::BurgerButton(this);
	burger->move(25, 25);

    auto minimize = new wall::MinimizeButton(this);
	minimize->move(475, 25);

    auto close = new wall::CloseButton(this);
	close->move(525, 25);

    auto wallpaper = new wall::WallpaperButton(this);
	wallpaper->setText("Ш П А Л Е Р А");
	wallpaper->move(375, 188);

    auto widget = new wall::WidgetButton(this);
	widget->setText("В І Д Ж Е Т");
	widget->move(375, 263);


	// Status
    auto wallStatus = new wall::WallpaperStatus(this);
	wallStatus->move(25, 197);
	wallStatus->setStatusText("WALLONE", "ПРИРОДА", "ЗИМОВИЙ ЗАХІД");

    auto notificationStatus = new wall::NotificationStatus(this);
	notificationStatus->move(25, 240);
	notificationStatus->setNotificationText("Сповіщення");
	notificationStatus->setNotificationCount(5);

    auto changedStatus = new wall::ChangedStatus(this);
	changedStatus->move(25, 289);
	changedStatus->setChangedText("2 години тому");


	// Main burger curtain
    auto mainBurger = new BurgerCurtain(this);
	mainBurger->setGeometry(0, 0, 600, 338);
	mainBurger->setHeaderText("М Е Н Ю");
	mainBurger->hide();

	mainBurger->setFirstButtonText("Авторизація");
	mainBurger->setSecondButtonText("Налаштування");
	mainBurger->setThirdButtonText("Про програму");
	mainBurger->setGreyText("Навіщо і що отримаю?");

    mainBurger->setFirstButtonIcon(":/res/Images/authorization.png");
    mainBurger->setSecondButtonIcon(":/res/Images/settings.png");
    mainBurger->setThirdButtonIcon(":/res/Images/about.png");


	// Widget form
    auto widgetEditor = new wall::Editor;
	widgetEditor->hide();


	// Conections
	connect(burger, SIGNAL(clicked()),
			mainBurger, SLOT(slotOpenBurgerCurtain()));

	connect(close, SIGNAL(clicked()),
			qApp, SLOT(quit()));

	connect(minimize, SIGNAL(clicked()),
			this, SLOT(showMinimized()));

	connect(widget, SIGNAL(clicked()),
			widgetEditor, SLOT(showFullScreen()));
}


//------------------------------------------------------------------------------
/*virtual*/ void MainForm::mousePressEvent(QMouseEvent *e)
{
    mNewFormPosition = e->pos();
    mIsPressed = true;
}

//------------------------------------------------------------------------------
/*virtual*/ void MainForm::mouseMoveEvent(QMouseEvent *e)
{
    if (mIsPressed) {
        this->move(e->globalPos() - mNewFormPosition);
	}
}

//------------------------------------------------------------------------------
/*virtual*/ void MainForm::mouseReleaseEvent(QMouseEvent *e)
{
    mIsPressed = false;
}

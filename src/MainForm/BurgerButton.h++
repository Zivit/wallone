#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class Burger
 * \brief Кнопка бургер.
 */
//==============================================================================
class BurgerButton : public QPushButton
{
public:
    BurgerButton(QWidget *parent = 0);
};

}

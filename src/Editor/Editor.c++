#include "Editor.h++"


namespace wall
{

//------------------------------------------------------------------------------
Editor::Editor(QWidget *parent) : QWidget(parent)
{
    // Настройка таблицы
    QDesktopWidget desktop;

    int w = 70;
    int h = 70;

    int cel = int((desktop.width() - w * 2) / 70);
    int row = int((desktop.height() - h * 2) / 70);

    QPoint pos;
    pos.setX(int((desktop.width() - cel * w) / 2));
    pos.setY(int((desktop.height() - row * h) / 2));

    mWallGrid = WallGrid(pos, cel, row, w, h);


    // Tool buttons
    QString buttonStyle = /* Кнопка */
            "QPushButton {"
            "background-color: #7ac5df;"
            "border-color: #7ac5df;"
            "border-style: solid;"
            "border-width: 1px;"
            "color: #ffffff;"
            "font: 16px / 24px \"PT Sans\";"
            "padding: 14px;"
            //"text-align: left;"
            "}"

            "QPushButton:hover {"
            "background-color: #6BBAD5;"
            "border-color: #7ac5df;"
            "color: #ffffff;"
            "font: 400 16px / 24px PTSans;"
            "}"

            "QPushButton:pressed {"
            "background-color: #8FD1E8;"
            "border-color: #7ac5df;"
            "color: #ffffff;"
            "font: 400 16px / 24px PTSans; "
            "}";
    QFont buttonFont("FontAwesome", 13);

    // Left buttons
    auto backgroundSettings = new QPushButton("");
    backgroundSettings->setFocusPolicy(Qt::NoFocus);
    backgroundSettings->setStyleSheet(buttonStyle);
    backgroundSettings->setFont(buttonFont);

    auto themeSettings = new QPushButton("");
    themeSettings->setFocusPolicy(Qt::NoFocus);
    themeSettings->setStyleSheet(buttonStyle);
    themeSettings->setFont(buttonFont);

    auto clearAll = new QPushButton("");
    clearAll->setFocusPolicy(Qt::NoFocus);
    clearAll->setStyleSheet(buttonStyle);
    clearAll->setFont(buttonFont);

    // Right buttons
    auto reflectHorizontal = new QPushButton("");
    reflectHorizontal->setFocusPolicy(Qt::NoFocus);
    reflectHorizontal->setStyleSheet(buttonStyle);
    reflectHorizontal->setFont(buttonFont);

    auto reflectVertical = new QPushButton("");
    reflectVertical->setFocusPolicy(Qt::NoFocus);
    reflectVertical->setStyleSheet(buttonStyle);
    reflectVertical->setFont(buttonFont);

    // Bottom buttons
    auto cancelButton = new QPushButton("");
    cancelButton->setFocusPolicy(Qt::NoFocus);
    cancelButton->setStyleSheet(buttonStyle);
    cancelButton->setFont(buttonFont);

    auto acceptButton = new QPushButton("");
    acceptButton->setFocusPolicy(Qt::NoFocus);
    acceptButton->setStyleSheet(buttonStyle);
    acceptButton->setFont(buttonFont);


    // Layouts setup
    auto leftLayout = new QVBoxLayout;
    leftLayout->addStretch(1);
    leftLayout->addWidget(backgroundSettings);
    leftLayout->addWidget(themeSettings);
    leftLayout->addWidget(clearAll);
    leftLayout->addStretch(1);

    auto rightLayout = new QVBoxLayout;
    rightLayout->addStretch(1);
    rightLayout->addWidget(reflectHorizontal);
    rightLayout->addWidget(reflectVertical);
    rightLayout->addStretch(1);

    auto centerLayout = new QHBoxLayout;
    centerLayout->addStretch(1);

    auto middleLayout = new QHBoxLayout;
    middleLayout->addLayout(leftLayout);
    middleLayout->addLayout(centerLayout);
    middleLayout->addLayout(rightLayout);

    auto topLayout = new QHBoxLayout;
    topLayout->addStretch(1);

    auto bottomLayout = new QHBoxLayout;
    bottomLayout->addStretch(1);
    bottomLayout->addWidget(cancelButton);
    bottomLayout->addWidget(acceptButton);
    bottomLayout->addStretch(1);

    auto mainLayout = new QVBoxLayout;
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(middleLayout);
    mainLayout->addLayout(bottomLayout);
    setLayout(mainLayout);


    // Создаем кнопки на сетке
    for (auto i = 0; i < cel; ++i) {
        for (auto j = 0; j < row; ++j) {
            auto plusButton = new QPushButton("+", this);
            plusButton->setGeometry(pos.x() + i * w, pos.y() + j * h,
                                    64, 64);
            plusButton->setFocusPolicy(Qt::NoFocus);
            plusButton->setStyleSheet("QPushButton { "
                                      "color: grey; "
                                      "border: solid 1px grey; "
                                      "}"
                                      "QPushButton:hover { "
                                      "color: white; "
                                      "background: lightgrey;"
                                      "border: 1px solid grey; "
                                      "}"
                                      "QPushButton:!hover { "
                                      "color: #01000000;"
                                      "}"
                                      );
            connect(plusButton, SIGNAL(clicked(bool)),
                    this, SLOT(on_addWidget()));
        }
    }

    wallWidgetVector.push_back(new WallWidgetDate(this));
    wallWidgetVector.back()->move(pos.x() + (0 * w), pos.y() + (0 * h));
    wallWidgetVector.back()->setGrid(mWallGrid);
    wallWidgetVector.back()->setVector(&wallWidgetVector);


    // Menu actions
    auto baseAction = new QAction("Base", this);
    connect(baseAction, SIGNAL(triggered()),
            SLOT(on_baseActionTriggered()));

    auto dateAction = new QAction("Date", this);
    connect(dateAction, SIGNAL(triggered()),
            SLOT(on_dateActionTriggered()));

    auto quoteAction = new QAction("Quote", this);
    connect(quoteAction, SIGNAL(triggered()),
            SLOT(on_quoteActionTriggered()));

    auto weatherAction = new QAction("Weather", this);
    connect(weatherAction, SIGNAL(triggered()),
            SLOT(on_weatherActionTriggered()));

    // Menu
    mAdderWidgetMenu = new QMenu(this);
    mAdderWidgetMenu->addAction(baseAction);
    mAdderWidgetMenu->addAction(dateAction);
    mAdderWidgetMenu->addAction(quoteAction);
    mAdderWidgetMenu->addAction(weatherAction);

    // Connections
    connect(cancelButton, SIGNAL(clicked()),
            SLOT(close()));
}

//------------------------------------------------------------------------------
void Editor::on_addWidget()
{
    mAddPoint = qobject_cast<QWidget*>(QObject::sender())->pos();

    mAdderWidgetMenu->popup(mAddPoint);
}

void Editor::mSetupWidget()
{
    wallWidgetVector.back()->move(mAddPoint);
    wallWidgetVector.back()->setGrid(mWallGrid);
    wallWidgetVector.back()->setVector(&wallWidgetVector);
    wallWidgetVector.back()->show();
}

//------------------------------------------------------------------------------
void Editor::on_baseActionTriggered()
{
    wallWidgetVector.push_back(new WallWidget(this));
    mSetupWidget();
}

//------------------------------------------------------------------------------
void Editor::on_dateActionTriggered()
{
    wallWidgetVector.push_back(new WallWidgetDate(this));
    mSetupWidget();
}

//------------------------------------------------------------------------------
void Editor::on_quoteActionTriggered()
{
    wallWidgetVector.push_back(new WallWidgetQuote(this));
    mSetupWidget();
}

//------------------------------------------------------------------------------
void Editor::on_weatherActionTriggered()
{
    wallWidgetVector.push_back(new WallWidgetWeather(this));
    mSetupWidget();
}

}

#pragma once

#include <QtWidgets>
#include <vector>

#include "src/Editor/WallGrid.h++"
#include "src/Editor/WallAnimateMove.h++"


/*!
 * \class WallWidget
 * \brief Базовый класс виджетов для рисования информации на обоях.
 */
//==============================================================================
class WallWidget : public QWidget
{
	Q_OBJECT
public:
	WallWidget(QWidget *parent = 0);

	virtual void mousePressEvent(QMouseEvent* mEvent) override;
	virtual void mouseMoveEvent(QMouseEvent* mEvent) override;
	virtual void mouseReleaseEvent(QMouseEvent* mEvent) override;

    void setGrid(WallGrid wallGrid);
	void setVector(std::vector<WallWidget*>* vector);
    void setKey(const QString& key);
	bool isCollision(QRect rect);

protected:
    QPoint mPressPosition;
    QPoint mOldPosition;
    QString mKey;
    bool mIsMousePressed = false;
    WallGrid mGrid;
    WallAnimateMove mAnimateMove;

    std::vector<WallWidget*>* mWallWidgetVector;

signals:
	void moved(QPoint point);
	void positionChanged(QPoint point);	

protected slots:
	virtual void on_widgetMove(QPoint point);
	void on_updatePoint(QPoint point);
	void on_align();
	void on_deleteWidget();
};

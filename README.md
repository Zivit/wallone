# WallOne (in development)

---

The program is designed for platforms Linux, Windows. It has to change wallpaper every day, and draw information selected in the program.

### Screenshots

*(UI Design by Bogdan Slomchinskiy)*

###### Main form

![Main form](https://bytebucket.org/Zivit/wallone/raw/5566ef340711e62a6a761c2dfaa47f359a189f23/Screenshot_MainForm.png)

###### Main menu

![Main menu](https://bytebucket.org/Zivit/wallone/raw/5566ef340711e62a6a761c2dfaa47f359a189f23/Screenshot_MainMenu.png)

###### Editor window

![Editor window](https://bytebucket.org/Zivit/wallone/raw/5566ef340711e62a6a761c2dfaa47f359a189f23/Screenshot_EditorWindow.png)
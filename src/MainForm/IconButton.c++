#include "IconButton.h++"


namespace wall
{

//------------------------------------------------------------------------------
IconButton::IconButton(QWidget *parent) : QPushButton(parent)
  , mTextColor("black")
  , mTextColorHover("black")
{
    mLabel = new QLabel(this);
    mLabel->setStyleSheet("QLabel {"
                         "	font: 16px PTSans;"
                         "}");
    mLabel->move(30 + this->iconSize().width(), -1);

    this->setFocusPolicy(Qt::NoFocus);
    this->setCursor(Qt::PointingHandCursor);
}


//------------------------------------------------------------------------------
void IconButton::setText(const QString& text)
{
    mLabel->setText(text);
}


//------------------------------------------------------------------------------
void IconButton::setIcon(const QString& fileName)
{
    mIconFileName = fileName;
    update();
}


//------------------------------------------------------------------------------
void IconButton::setTextColor(const QString& color)
{
    mTextColor = color;
    //update();
}


//------------------------------------------------------------------------------
void IconButton::mUpdate()
{
    this->setStyleSheet("QPushButton {"
                        "	border: 0px;"
                        "	image: url(" + mIconFileName +");"
                        "	image-position: left;"
                        "}"
                        ""
                        "QLabel {"
                        "	color: " + mTextColor + ";"
                        "}"
                        );

}

}

#include "WallWidgetDate.h++"


//------------------------------------------------------------------------------
WallWidgetDate::WallWidgetDate(QWidget* parent)
	: WallWidget(parent)
{
	QPalette pal;
	pal.setColor(this->backgroundRole(), QColor(225, 158, 55));

    QSize widgetSize(134, 134);
    this->setFixedSize(widgetSize);

    mWidgetImage = new QImage(widgetSize,
                             QImage::Format_ARGB32_Premultiplied);

    mCreateTile();

	QBrush br;
    br.setTextureImage(*mWidgetImage);
	pal.setBrush(this->backgroundRole(), br);

	this->setPalette(pal);
	this->setAutoFillBackground(true);

	connect(this, SIGNAL(moved(QPoint)), SLOT(on_widgetMove(QPoint)));

    connect(&mAnimateMove, SIGNAL(finishAnimate()),
			this, SLOT(on_align()));

    connect(&mAnimateMove, SIGNAL(updatePoint(QPoint)),
			this, SLOT(on_updatePoint(QPoint)));
}

//------------------------------------------------------------------------------
void WallWidgetDate::on_widgetMove(QPoint point)
{
    this->move(mapToParent(point) - mPressPosition);
}

//------------------------------------------------------------------------------
void WallWidgetDate::mCreateTile()
{
	// Date former
    auto nowDay = QString::number(QDate::currentDate().day());

    auto dayOfWeek = QDate::currentDate().dayOfWeek();
    QString nowWeekDay = (dayOfWeek == 1) ? tr("Wednesday") :
                         (dayOfWeek == 2) ? tr("Tuesday") :
                         (dayOfWeek == 3) ? tr("Wednesday") :
                         (dayOfWeek == 4) ? tr("Thursday") :
                         (dayOfWeek == 5) ? tr("Friday") :
                         (dayOfWeek == 6) ? tr("Suturday") :
                         (dayOfWeek == 7) ? tr("Sunday") : "Invalid weekday";


    auto month = QDate::currentDate().month();
    QString nowMonth = (month == 1) ? tr("January") :
                       (month == 2) ? tr("February") :
                       (month == 3) ? tr("March") :
                       (month == 4) ? tr("April") :
                       (month == 5) ? tr("May") :
                       (month == 6) ? tr("Jule") :
                       (month == 7) ? tr("June") :
                       (month == 8) ? tr("August") :
                       (month == 9) ? tr("September") :
                       (month == 10) ? tr("October") :
                       (month == 11) ? tr("November") :
                       (month == 12) ? tr("December") : "Invalid month";


	QPainter paint;
	/////
    paint.begin(mWidgetImage);

	QRect boxBack = QRect(0, 0, width(), height());
	paint.setPen(QColor(70, 70, 70));
	paint.setBrush(QColor(120, 120, 120));
	//paint.setOpacity(0.3);
	paint.drawRect(boxBack);

	QRect boxText = QRect(10, 10, 114, 114);
	//paint.setOpacity(1);
	paint.setPen(QColor(255, 255, 255));
	paint.setFont(QFont("Monospace", 12));
	paint.drawText(boxText, Qt::AlignHCenter | Qt::AlignTop, nowWeekDay);

	paint.setPen(QColor(255, 255, 255));
	paint.setFont(QFont("Monospace", 48));
	paint.drawText(boxText, Qt::AlignCenter, nowDay);

	paint.setPen(QColor(255, 255, 255));
	paint.setFont(QFont("Monospace", 12));
	paint.drawText(boxText, Qt::AlignHCenter | Qt::AlignBottom, nowMonth);


	paint.end();
	/////
}

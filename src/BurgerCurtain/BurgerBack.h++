#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class BurgerBack
 * \brief Класс обертка для QPushButton. Кнопка для закрытия шторки.
 */
//==============================================================================
class BurgerBack : public QPushButton
{
public:
    BurgerBack(QWidget *parent = 0);
};

}

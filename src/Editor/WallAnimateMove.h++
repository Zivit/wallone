#pragma once

#include <QtWidgets>


/*!
 * \class WallAnimateMove
 * \brief Клас для создания анимации движения виджетов при перемещении.
 */
//==============================================================================
class WallAnimateMove : public QWidget
{
	Q_OBJECT
public:
	WallAnimateMove(QWidget* parent = 0);

    void setObject(QPoint source, QPoint target, int duration = 2000);

	void run();
	void stop();

private:
    QTimer* mTimer; // interval 25ms (~40fps)
    QPoint mSourcePoint;
    QPoint mTargetPoint;
    float mTime; // ms
    float mCounter;

signals:
	void finishAnimate();
	void updatePoint(QPoint);

public slots:
	void on_update();
};

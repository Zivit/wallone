#pragma once

#include <QtWidgets>

#include "WallWidget.h++"


/*!
 * \class WallWidgetQuote
 * \brief Виджет цитаты.
 */
//==============================================================================
class WallWidgetQuote : public WallWidget
{
	Q_OBJECT
public:
	WallWidgetQuote(QWidget* parent = 0);

private:
    QImage* mWidgetImage;
    void mCreateTile();

signals:
	void moved(QPoint point);
	void positionChanged(QPoint point);

protected slots:
	virtual void on_widgetMove(QPoint point) override;
};

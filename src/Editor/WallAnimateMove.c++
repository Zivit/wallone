#include "WallAnimateMove.h++"


//------------------------------------------------------------------------------
WallAnimateMove::WallAnimateMove(QWidget* parent)
	: QWidget(parent)
{
    mTimer = new QTimer(this);
    mTime = 2000;
    mCounter = 0;

    connect(mTimer, SIGNAL(timeout()), SLOT(on_update()));
}

//------------------------------------------------------------------------------
void WallAnimateMove::on_update()
{
    if (mCounter < mTime) {

        float x = (mSourcePoint.x() +
                  (mCounter / mTime) *
                  (mTargetPoint.x() - mSourcePoint.x()));

        float y = (mSourcePoint.y() +
                  (mCounter / mTime) *
                  (mTargetPoint.y() - mSourcePoint.y()));

        emit updatePoint(QPoint(x, y));

        mCounter += mTimer->interval();
		run();
	}
	else {
        mCounter = 0;
		stop();
		emit finishAnimate();
	}
}

//------------------------------------------------------------------------------
void WallAnimateMove::setObject(QPoint source, QPoint target, int duration)
{
    mSourcePoint = source;
    mTargetPoint = target;
    mTime = duration;
}

//------------------------------------------------------------------------------
void WallAnimateMove::run()
{
    mTimer->start(5);
}

//------------------------------------------------------------------------------
void WallAnimateMove::stop()
{
    mTimer->stop();
}

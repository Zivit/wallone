#include "CloseButton.h++"


namespace wall
{

//------------------------------------------------------------------------------
CloseButton::CloseButton(QWidget* parent) : QPushButton(parent)
{
    this->setStyleSheet("QPushButton {"
                        "	width: 50px;"
                        "	height: 50px;"
                        "	border: 1px solid #ffd409;"
                        "	border-left-color: #00ffffff;"
                        "	image: url(:/res/Images/close.png);"
                        "}"
                        ""
                        "QPushButton:hover {"
                        "	image: url(:/res/Images/close_hovered.png);"
                        "}");

    this->setFocusPolicy(Qt::NoFocus);
}

}



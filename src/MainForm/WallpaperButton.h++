#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class WallpaperButton
 * \brief Кнопка выбора обоев.
 */
//==============================================================================
class WallpaperButton : public QPushButton
{
public:
    WallpaperButton(QWidget *parent = 0);
};

}

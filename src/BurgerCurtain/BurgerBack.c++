#include "BurgerBack.h++"


namespace wall
{

//------------------------------------------------------------------------------
BurgerBack::BurgerBack(QWidget *parent) : QPushButton(parent)
{
    this->setStyleSheet("QPushButton {"
                        "	width: 50px;"
                        "	height: 50px;"
                        "	border: 1px solid #bcbcbc;"
                        "	image: url(:/res/Images/burgerBack.png);"
                        "}"
                        ""
                        "QPushButton:hover {"
                        "	image: url(:/res/Images/burgerBack_hovered.png);"
                        "	border: 1px solid #7ac5df;"
                        "}");

    this->setFocusPolicy(Qt::NoFocus);
}

}

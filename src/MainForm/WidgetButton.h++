#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class WidgetButton
 * \brief Кнопка открытия редактора виджетов.
 */
//==============================================================================
class WidgetButton : public QPushButton
{
public:
    WidgetButton(QWidget *parent = 0);
};

}

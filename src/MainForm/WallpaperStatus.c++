#include "WallpaperStatus.h++"


namespace wall
{

//------------------------------------------------------------------------------
WallpaperStatus::WallpaperStatus(QWidget *parent) : QWidget(parent)
{
    this->setStyleSheet("QLabel {"
                        "	color: #7ac5df;"
                        "	font: bold 12px PTSans;"
                        "}");

    mCollectionLabel = new QLabel(this);

    mEllipse1 = new QLabel(this);
    mEllipse1->setPixmap(QPixmap(":/res/Images/whiteEllipse"));

    mCategoryLabel = new QLabel(this);

    mEllipse2 = new QLabel(this);
    mEllipse2->setPixmap(QPixmap(":/res/Images/whiteEllipse"));

    mFileNameLabel = new QLabel(this);
}

//------------------------------------------------------------------------------
void WallpaperStatus::setStatusText(const QString& collection,
                                    const QString& category,
                                    const QString& fileName)
{
    mCollectionLabel->setText(collection);

    mEllipse1->move(mCollectionLabel->x() + mCollectionLabel->width() / 2 + 9, 6);

    mCategoryLabel->setText(category);
    mCategoryLabel->move(mEllipse1->x() + 4 + 9, 0);

    mEllipse2->move(mCategoryLabel->x() + mCategoryLabel->width() / 2 + 9, 6);

    mFileNameLabel->setText(fileName);
    mFileNameLabel->move(mEllipse2->x() + 4 + 9, 0);
}

}

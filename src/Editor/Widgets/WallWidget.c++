#include "WallWidget.h++"


//------------------------------------------------------------------------------
WallWidget::WallWidget(QWidget *parent)
	: QWidget(parent)
{
	QPalette pal;
	pal.setColor(this->backgroundRole(), QColor(25, 158, 255));

	QSize m_widgetSize(64, 64);
	this->setFixedSize(m_widgetSize);

	this->setPalette(pal);
	this->setAutoFillBackground(true);


	QFont closeFont("FontAwesome", 13);

    auto closeButton = new QPushButton("", this);
	closeButton->setFocusPolicy(Qt::NoFocus);
	closeButton->setFont(closeFont);
	closeButton->setFlat(true);
	closeButton->setGeometry(0, 0, 25, 25);
	closeButton->setStyleSheet("QPushButton { color: white; }"
							   "QPushButton:hover { color: white; }"
							   "QPushButton:!hover { color: #1f000000;}"
							   );


    auto settingButton = new QPushButton("", this);
	settingButton->setFocusPolicy(Qt::NoFocus);
	settingButton->setFont(closeFont);
	settingButton->setStyleSheet("QPushButton { color: white }");
	settingButton->setFlat(true);
	settingButton->setGeometry(0, 25, 25, 25);
	settingButton->setStyleSheet("QPushButton { color: white; }"
							   "QPushButton:hover { color: white; }"
							   "QPushButton:!hover { color: #1f000000;}"
							   );

	connect(closeButton, SIGNAL(clicked(bool)),
			this, SLOT(on_deleteWidget()));

	connect(this, SIGNAL(moved(QPoint)), SLOT(on_widgetMove(QPoint)));

    connect(&mAnimateMove, SIGNAL(finishAnimate()),
			this, SLOT(on_align()));

    connect(&mAnimateMove, SIGNAL(updatePoint(QPoint)),
			this, SLOT(on_updatePoint(QPoint)));


    auto dropShadow = new QGraphicsDropShadowEffect;
	dropShadow->setBlurRadius(8);
	dropShadow->setOffset(0, 1);

    this->setGraphicsEffect(dropShadow);
}

//------------------------------------------------------------------------------
void WallWidget::on_deleteWidget()
{
	this->setVisible(false);

    for (auto i = mWallWidgetVector->begin();
         i != mWallWidgetVector->end();
		 ++i)
	{
		if (!(*i)->isVisible()) {
            mWallWidgetVector->erase(i);
			break;
		}
	}

	this->close();
}

//------------------------------------------------------------------------------
void WallWidget::on_updatePoint(QPoint point)
{
    this->move(point);
}

//------------------------------------------------------------------------------
void WallWidget::on_align()
{
    this->move(mGrid.getAlignPoint((pos() + QPoint(25, 25))));
}

//------------------------------------------------------------------------------
void WallWidget::mousePressEvent(QMouseEvent* mEvent)
{
	if (mEvent->button() == Qt::LeftButton) {
        mPressPosition = mEvent->pos();
        mOldPosition = pos();
        mIsMousePressed = true;
	}
}

//------------------------------------------------------------------------------
void WallWidget::mouseMoveEvent(QMouseEvent* mEvent)
{
    if (mIsMousePressed) {
		emit moved(mEvent->pos());
	}
}

//------------------------------------------------------------------------------
void WallWidget::mouseReleaseEvent(QMouseEvent* mEvent)
{
	if (mEvent->button() == Qt::LeftButton) {
        mIsMousePressed = false;
        QPoint relPoint(x() + 32, y() + 32);

		if (isCollision(QRect(x(), y(), width(), height()))) {
            mAnimateMove.setObject(QPoint(x(), y()), mOldPosition, 200);
            mAnimateMove.run();
            emit positionChanged(mOldPosition);
			return;
		}
		else {
            move(mGrid.getAlignPoint(relPoint));
		}

		emit positionChanged(this->pos());
	}
}

//------------------------------------------------------------------------------
void WallWidget::on_widgetMove(QPoint point)
{
    this->move(mapToParent(point) - mPressPosition);
}

//------------------------------------------------------------------------------
void WallWidget::setGrid(WallGrid wallGrid)
{
    mGrid = wallGrid;
}

//------------------------------------------------------------------------------
void WallWidget::setVector(std::vector<WallWidget*>* vector)
{
    mWallWidgetVector = vector;
}

//------------------------------------------------------------------------------
void WallWidget::setKey(const QString& key)
{
    mKey = key;
}

//------------------------------------------------------------------------------
bool WallWidget::isCollision(QRect rect)
{
    if (!QRect(mGrid.position().x(),
               mGrid.position().y(),
               mGrid.allWidth(),
               mGrid.allHeight()).contains(rect)) {
		return true;
	}

    for (auto& item : *mWallWidgetVector) {
        QRect secondRect = QRect(item->x(), item->y(),
                                 item->width(), item->height());

		if (secondRect == rect) continue;

		if (secondRect.intersects(rect)) {
			return true;
		}
	}
	return false;
}

#include "WallWidgetWeather.h++"


//------------------------------------------------------------------------------
WallWidgetWeather::WallWidgetWeather(QWidget* parent)
	: WallWidget(parent)
{
	QPalette pal;
	pal.setColor(this->backgroundRole(), QColor(225, 158, 55));

    QSize widgetSize(134, 134);
    this->setFixedSize(widgetSize);

    mWidgetImage = new QImage(widgetSize,
                             QImage::Format_ARGB32_Premultiplied);

    mCreateTile();

	QBrush br;
    br.setTextureImage(*mWidgetImage);
	pal.setBrush(this->backgroundRole(), br);

	this->setPalette(pal);
	this->setAutoFillBackground(true);

	connect(this, SIGNAL(moved(QPoint)), SLOT(on_widgetMove(QPoint)));
}

//------------------------------------------------------------------------------
void WallWidgetWeather::on_widgetMove(QPoint point)
{
    this->move(mapToParent(point) - mPressPosition);
}

//------------------------------------------------------------------------------
void WallWidgetWeather::mCreateTile()
{
	// Погода
    QPixmap weatherPix(":/res/Images/weather.png");
    QString weatherText = "+25 °С";
    QString cityText = "Kiev";

	// Рисовач
	QPainter paint;
	/////
    paint.begin(mWidgetImage);

	QRect boxBack = QRect(0, 0, width(), height());
	paint.setPen(QColor(70, 70, 70));
	paint.setBrush(QColor(120, 120, 120));
	paint.drawRect(boxBack);

	QRect boxText = QRect(10, 10, width() - 20, height() - 20);

	paint.drawPixmap(20, 25, weatherPix);
	paint.setPen(QColor(255, 255, 255));
	paint.setFont(QFont("Monospace", 12));
	paint.drawText(boxText, Qt::AlignBottom | Qt::AlignHCenter, weatherText);
	paint.drawText(boxText, Qt::AlignTop | Qt::AlignHCenter, cityText);


	paint.end();
	/////
}

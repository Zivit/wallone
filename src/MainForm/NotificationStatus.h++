#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class NotificationStatus
 * \brief Оповещения программы.
 */
//==============================================================================
class NotificationStatus : public QWidget
{
    Q_OBJECT
public:
    NotificationStatus(QWidget *parent = 0);

private:
    QLabel* mNotificationText;
    QLabel* mNotificationCount;

public slots:
    void setNotificationText(const QString& text);
    void setNotificationCount(const int& count);
};

}

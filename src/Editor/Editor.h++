#pragma once

#include <QtWidgets>
#include <memory>
#include <map>
#include <QVector>

#include "WallGrid.h++"
#include "Widgets/WallWidget.h++"
#include "Widgets/WallWidgetDate.h++"
#include "Widgets/WallWidgetQuote.h++"
#include "Widgets/WallWidgetWeather.h++"


namespace wall
{

/*!
 * \class Editor
 * \brief Клас для размещения виджетов на обоях.
 */
//==============================================================================
class Editor : public QWidget
{
    Q_OBJECT
public:
    Editor(QWidget *parent = 0);

    std::vector<WallWidget*> wallWidgetVector;

private:
    WallGrid mWallGrid;
    QMenu* mAdderWidgetMenu;
    QPoint mAddPoint;

    void mSetupWidget();

public slots:
    void on_addWidget();
    void on_baseActionTriggered();
    void on_dateActionTriggered();
    void on_quoteActionTriggered();
    void on_weatherActionTriggered();
};

}

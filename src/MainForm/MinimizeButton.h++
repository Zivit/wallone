#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class MinimizeButton
 * \brief Копка свертывания главного окна.
 */
//==============================================================================
class MinimizeButton : public QPushButton
{
public:
    MinimizeButton(QWidget *parent = 0);
};

}

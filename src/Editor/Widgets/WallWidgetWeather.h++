#pragma once

#include <QtWidgets>

#include "WallWidget.h++"


/*!
 * \class WallWidgetWeather
 * \brief Виджет погоды.
 */
//==============================================================================
class WallWidgetWeather : public WallWidget
{
	Q_OBJECT
public:
	WallWidgetWeather(QWidget* parent = 0);

private:
    QImage* mWidgetImage;
    void mCreateTile();

signals:
	void moved(QPoint point);
	void positionChanged(QPoint point);

protected slots:
	virtual void on_widgetMove(QPoint point) override;
};

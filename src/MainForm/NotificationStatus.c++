#include "NotificationStatus.h++"


namespace wall
{

//------------------------------------------------------------------------------
NotificationStatus::NotificationStatus(QWidget *parent) : QWidget(parent)
{
    auto notificationIcon = new QLabel(this);
    notificationIcon->setPixmap(QPixmap(":/res/Images/notificationsStatus.png"));

    auto yellowEllipse = new QLabel(this);
    yellowEllipse->setPixmap(QPixmap(":/res/Images/yellowEllipse.png"));
    yellowEllipse->move(9, 0);

    //! \todo Исправить шрифт
    mNotificationCount = new QLabel(this);
    mNotificationCount->resize(20, 20);
    mNotificationCount->move(9, 0);
    mNotificationCount->setAlignment(Qt::AlignCenter);
    mNotificationCount->setStyleSheet("QLabel {"
                                       "	color: #2e3640;"
                                       "	font: 13px PTSans;"
                                       "}");

    mNotificationText = new QLabel(this);
    mNotificationText->move(49, 0);
    mNotificationText->setStyleSheet("QLabel {"
                                      "	color: #ffffff;"
                                      "	font: bold 16px PTSans;"
                                      "}"
                                      ""
                                      "QLabel:hover {"
                                      "	color: #ffd409;"
                                      "}");
}

//------------------------------------------------------------------------------
void NotificationStatus::setNotificationText(const QString& text)
{
    mNotificationText->setText(text);
}

//------------------------------------------------------------------------------
void NotificationStatus::setNotificationCount(const int& count)
{
    if (count == 0) mNotificationCount->hide();

    mNotificationCount->setText(QString::number(count));
}
}

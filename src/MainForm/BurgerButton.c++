#include "BurgerButton.h++"


namespace wall
{

//------------------------------------------------------------------------------
BurgerButton::BurgerButton(QWidget *parent) : QPushButton(parent)
{
    this->setStyleSheet("QPushButton {"
                        "	width: 50px;"
                        "	height: 50px;"
                        "	border: 1px solid #ffd409;"
                        "	image: url(:/res/Images/burger.png);"
                        "}"
                        ""
                        "QPushButton:hover {"
                        "	image: url(:/res/Images/burger_hovered.png);"
                        "}");

    this->setFocusPolicy(Qt::NoFocus);
}

}

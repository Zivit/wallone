#include "BurgerCurtain.h++"


//------------------------------------------------------------------------------
BurgerCurtain::BurgerCurtain(QWidget *parent) : QWidget(parent)
{
    mIsShow = false;

	// Grey background //
    mBackgroundWidget = new QWidget(this);
    mBackgroundWidget->setGeometry(0, 0, 600, 338);
    mBackgroundWidget->setStyleSheet("QWidget {"
									  "	background-color: rgba(46, 54, 64, 0.95);"
									  "}");

    auto backOpacity = new QGraphicsOpacityEffect;
	backOpacity->setOpacity(0.0);
    mBackgroundWidget->setGraphicsEffect(backOpacity);


	// Grey background animation //
	int duration = 300;

    mBackAnimationHide = new QPropertyAnimation(backOpacity, "opacity");
    mBackAnimationHide->setDuration(duration / 2);
    mBackAnimationHide->setStartValue(1.0);
    mBackAnimationHide->setEndValue(0.0);

    mBackAnimationShow = new QPropertyAnimation(backOpacity, "opacity");
    mBackAnimationShow->setDuration(duration);
    mBackAnimationShow->setStartValue(0.0);
    mBackAnimationShow->setEndValue(1.0);


	// Curtain //
    auto curtainMenu = new QWidget(this);
	curtainMenu->setGeometry(0, 0, 300, 338);
	curtainMenu->setStyleSheet("QWidget {"
							   "	background-color: rgba(255, 255, 255, 1);"
							   "}");


	// Curtain elements //
    mCurtainHeader = new QLabel(curtainMenu);
    mCurtainHeader->setStyleSheet("QLabel {"
								   "	color: #bcbcbc;"
								   "	font: 13px PTSans;"
								   "}");
    mCurtainHeader->setFixedWidth(190);
    mCurtainHeader->move(84, 46);
    mCurtainHeader->setAlignment(Qt::AlignRight);
    mCurtainHeader->setText("H E A D E R");


	// Curtain animation //
    mCurtainAnimationHide = new QPropertyAnimation(curtainMenu, "geometry");
    mCurtainAnimationHide->setDuration(duration / 2);
    mCurtainAnimationHide->setStartValue(QRect(0, 0, 300, 338));
    mCurtainAnimationHide->setEndValue(QRect(-300, 0, 0, 338));

    mCurtainAnimationShow = new QPropertyAnimation(curtainMenu, "geometry");
    mCurtainAnimationShow->setDuration(duration);
    mCurtainAnimationShow->setStartValue(QRect(-300, 0, 0, 338));
    mCurtainAnimationShow->setEndValue(QRect(0, 0, 300, 338));


	// Paralel animations //
    mParalelHide = new QParallelAnimationGroup;
    mParalelHide->addAnimation(mBackAnimationHide);
    mParalelHide->addAnimation(mCurtainAnimationHide);

    mParalelShow = new QParallelAnimationGroup;
    mParalelShow->addAnimation(mBackAnimationShow);
    mParalelShow->addAnimation(mCurtainAnimationShow);


	// Controls //
    auto burgerBack = new wall::BurgerBack(curtainMenu);
	burgerBack->move(25, 25);

    auto greyLine = new QLabel(curtainMenu);
    greyLine->setPixmap(QPixmap(":/res/Images/greyLine.png"));
	greyLine->setGeometry(0, 138, 6, 75);

    mFirstButton = new wall::IconButton(curtainMenu);
    mFirstButton->setTextColor("#7ac5df");
    mFirstButton->move(25, 140);
    mFirstButton->setFixedWidth(250);
    mFirstButton->setFixedHeight(20);

    mGreyText = new QPushButton(curtainMenu);
    mGreyText->setStyleSheet("QPushButton {"
							"	text-align: left;"
							"	border: 0px;"
							"	font: 16px PTSans;"
							"	color: #bcbcbc;"
							"}");
    mGreyText->move(25, 191);
    mGreyText->setFocusPolicy(Qt::NoFocus);
    mGreyText->setCursor(Qt::PointingHandCursor);

    mSecondButton = new wall::IconButton(curtainMenu);
    mSecondButton->setTextColor("#7ac5df");
    mSecondButton->move(25, 240);
    mSecondButton->setFixedWidth(250);
    mSecondButton->setFixedHeight(20);

    mThirdButton = new wall::IconButton(curtainMenu);
    mThirdButton->setTextColor("#7ac5df");
    mThirdButton->move(25, 290);
    mThirdButton->setFixedWidth(250);
    mThirdButton->setFixedHeight(20);


	// Connections //
	// Hide curtain
    connect(mBackAnimationHide, SIGNAL(finished()),
			this, SLOT(hide()));

	// Close curtain
	connect(burgerBack, SIGNAL(clicked()),
			this, SLOT(slotCloseBurgerCurtain()));

}

//------------------------------------------------------------------------------
void BurgerCurtain::slotOpenBurgerCurtain()
{
    if (!mIsShow) {
		this->show();
        mParalelShow->start();
        mIsShow = true;
	}
}

//------------------------------------------------------------------------------
void BurgerCurtain::slotCloseBurgerCurtain()
{
    if (mIsShow) {
        mParalelHide->start();
        mIsShow = false;
	}
}

//------------------------------------------------------------------------------
void BurgerCurtain::setHeaderText(const QString &text)
{
    mCurtainHeader->setText(text);
}

//------------------------------------------------------------------------------
void BurgerCurtain::setFirstButtonText(const QString &text)
{
    mFirstButton->setText(text);
}

//------------------------------------------------------------------------------
void BurgerCurtain::setSecondButtonText(const QString &text)
{
    mSecondButton->setText(text);
}

//------------------------------------------------------------------------------
void BurgerCurtain::setThirdButtonText(const QString &text)
{
    mThirdButton->setText(text);
}

//------------------------------------------------------------------------------
void BurgerCurtain::setFirstButtonIcon(const QString &fileName)
{
    mFirstButton->setIcon(fileName);
}

//------------------------------------------------------------------------------
void BurgerCurtain::setSecondButtonIcon(const QString &fileName)
{
    mSecondButton->setIcon(fileName);
}

//------------------------------------------------------------------------------
void BurgerCurtain::setThirdButtonIcon(const QString &fileName)
{
    mThirdButton->setIcon(fileName);
}

//------------------------------------------------------------------------------
void BurgerCurtain::setGreyText(const QString &text)
{
    mGreyText->setText(text);
}

//------------------------------------------------------------------------------
wall::IconButton* BurgerCurtain::getFirstButton()
{
    return mFirstButton;
}

//------------------------------------------------------------------------------
wall::IconButton* BurgerCurtain::getSecondButton()
{
    return mSecondButton;
}

//------------------------------------------------------------------------------
wall::IconButton* BurgerCurtain::getThirdButton()
{
    return mThirdButton;
}

//------------------------------------------------------------------------------
QPushButton* BurgerCurtain::getGreyText()
{
    return mGreyText;
}


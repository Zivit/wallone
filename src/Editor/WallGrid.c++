#include "WallGrid.h++"


//------------------------------------------------------------------------------
WallGrid::WallGrid(QPoint pos, int col, int row, int w, int h)
{
    mPosition = pos;
    mColumns = col;
    mRows = row;
    mWidth = w;
    mHeight = h;

    mAllWidth = pos.x() + col * w;
    mAllHeight = pos.y() + row * h;
    qDebug() << "Grid size: " << mAllWidth << "x" << mAllHeight;

    for (int i = 0; i < mColumns; ++i) {
        for (int j = 0; j < mRows; ++j) {
			QRect rect;
            rect.setX(pos.x() + i * mWidth);
            rect.setY(pos.y() + j * mHeight);
            rect.setWidth(mWidth);
            rect.setHeight(mHeight);
            mGridRects.push_back(rect);
		}
	}
}

//------------------------------------------------------------------------------
bool WallGrid::isContains(QPoint position, QRect rect)
{
    if (rect.x() < position.x() && position.x() < rect.topRight().x() &&
        rect.y() < position.y() && position.y() < rect.bottomLeft().y() + 1 )
	{
		return true;
	}
	return false;
}


//------------------------------------------------------------------------------
QPoint WallGrid::getAlignPoint(QPoint position)
{
    for (auto rect : mGridRects) {
        if (isContains(position, rect)) {
			return rect.topLeft();
		}
	}
    return position;
}

//------------------------------------------------------------------------------
QPoint WallGrid::position() const
{
    return mPosition;
}

//------------------------------------------------------------------------------
int WallGrid::columns() const
{
    return mColumns;
}

//------------------------------------------------------------------------------
int WallGrid::rows() const
{
    return mRows;
}

//------------------------------------------------------------------------------
int WallGrid::width() const
{
    return mWidth;
}

//------------------------------------------------------------------------------
int WallGrid::height() const
{
    return mHeight;
}

//------------------------------------------------------------------------------
int WallGrid::allWidth() const
{
    return mAllWidth;
}

//------------------------------------------------------------------------------
int WallGrid::allHeight() const
{
    return mAllHeight;
}

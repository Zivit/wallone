#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class WallpaperStatus
 * \brief Показывает название обоев.
 */
//==============================================================================
class WallpaperStatus : public QWidget
{
    Q_OBJECT
public:
    WallpaperStatus(QWidget *parent = 0);

private:
    QLabel* mCollectionLabel;
    QLabel* mCategoryLabel;
    QLabel* mFileNameLabel;
    QLabel* mEllipse1;
    QLabel* mEllipse2;

public slots:
    void setStatusText(const QString& collection,
                       const QString& category,
                       const QString& fileName);
};

}

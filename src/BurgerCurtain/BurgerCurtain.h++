#pragma once

#include <QtWidgets>

#include "BurgerBack.h++"
#include "src/MainForm/IconButton.h++"


/*!
 * \class BurgerCurtain
 * \brief "Шторка" меню.
 */
//==============================================================================
class BurgerCurtain : public QWidget
{
	Q_OBJECT
public:
	explicit BurgerCurtain(QWidget *parent = 0);

	void setFirstButtonText(const QString &text);
	void setSecondButtonText(const QString &text);
	void setThirdButtonText(const QString &text);

	void setFirstButtonIcon(const QString &text);
	void setSecondButtonIcon(const QString &text);
	void setThirdButtonIcon(const QString &text);

	void setGreyText(const QString &text);

    wall::IconButton* getFirstButton();
    wall::IconButton* getSecondButton();
    wall::IconButton* getThirdButton();
	QPushButton* getGreyText();

private:
    QPropertyAnimation* mBackAnimationHide;
    QPropertyAnimation* mBackAnimationShow;
    QPropertyAnimation* mCurtainAnimationHide;
    QPropertyAnimation* mCurtainAnimationShow;
    QParallelAnimationGroup* mParalelHide;
    QParallelAnimationGroup* mParalelShow;

    wall::IconButton* mFirstButton;
    QPushButton* mGreyText;
    wall::IconButton* mSecondButton;
    wall::IconButton* mThirdButton;

    QWidget* mBackgroundWidget;
    QLabel* mCurtainHeader;
    bool mIsShow;

signals:

public slots:
	void slotOpenBurgerCurtain();
	void slotCloseBurgerCurtain();
	void setHeaderText(const QString &text);
};


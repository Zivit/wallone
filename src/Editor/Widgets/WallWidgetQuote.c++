#include "WallWidgetQuote.h++"


//------------------------------------------------------------------------------
WallWidgetQuote::WallWidgetQuote(QWidget* parent)
	: WallWidget(parent)
{
	QPalette pal;
	pal.setColor(this->backgroundRole(), QColor(225, 158, 55));

    QSize widgetSize(414, 64);
    this->setFixedSize(widgetSize);

    mWidgetImage = new QImage(widgetSize,
                             QImage::Format_ARGB32_Premultiplied);

    mCreateTile();

	QBrush br;
    br.setTextureImage(*mWidgetImage);
	pal.setBrush(this->backgroundRole(), br);

	this->setPalette(pal);
	this->setAutoFillBackground(true);

	connect(this, SIGNAL(moved(QPoint)), SLOT(on_widgetMove(QPoint)));
}

//------------------------------------------------------------------------------
void WallWidgetQuote::on_widgetMove(QPoint point)
{
    this->move(mapToParent(point) - mPressPosition);
}

//------------------------------------------------------------------------------
void WallWidgetQuote::mCreateTile()
{
	// Цитата
    QString quoteText = "\"Коли Мучник б'ється з сажотрусом,\n"
                        "Мучник стає чорним від сажі, \n"
                        "а сажотрус виглядає чистіше.\"";


	// Рисовач
	QPainter paint;
	/////
    paint.begin(mWidgetImage);

	QRect boxBack = QRect(0, 0, width(), height());
	paint.setPen(QColor(70, 70, 70));
	paint.setBrush(QColor(120, 120, 120));
	paint.drawRect(boxBack);

	QRect boxText = QRect(10, 10, 394, 44);
	paint.setPen(QColor(255, 255, 255));
	paint.setFont(QFont("Monospace", 10));
	paint.drawText(boxText, Qt::AlignCenter, quoteText);


	paint.end();
	/////
}

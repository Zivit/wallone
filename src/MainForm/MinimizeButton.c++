#include "MinimizeButton.h++"


namespace wall
{

//------------------------------------------------------------------------------
MinimizeButton::MinimizeButton(QWidget *parent) : QPushButton(parent)
{
    this->setStyleSheet("QPushButton {"
                        "	width: 50px;"
                        "	height: 50px;"
                        "	border: 1px solid #ffd409;"
                        "	border-right-color: #00ffffff;"
                        "	image: url(:/res/Images/minimize.png);"
                        "}"
                        ""
                        "QPushButton:hover {"
                        "	image: url(:/res/Images/minimize_hovered.png);"
                        "}");

    this->setFocusPolicy(Qt::NoFocus);
}

}


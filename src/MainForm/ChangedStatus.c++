#include "ChangedStatus.h++"


namespace wall
{

//------------------------------------------------------------------------------
ChangedStatus::ChangedStatus(QWidget *parent) : QWidget(parent)
{
    auto changedTextIcon = new QLabel(this);
    changedTextIcon->setPixmap(QPixmap(":/res/Images/changedStatus.png"));

    //! Исправить шрифт
    mChangedText = new QLabel(this);
    mChangedText->move(50, 0);
    mChangedText->setStyleSheet("QLabel {"
                               "    color: #ffffff;"
                               "	font: bold 16px PTSans;"
                               "}"
                               ""
                               "QLabel:hover {"
                               "	color: #ffd409;"
                               "}");
}

//------------------------------------------------------------------------------
void ChangedStatus::setChangedText(const QString& text)
{
    mChangedText->setText(text);
}

}

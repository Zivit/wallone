#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class ChangedStatus
 * \brief Время изменения информации на обоях.
 */
//==============================================================================
class ChangedStatus : public QWidget
{
    Q_OBJECT
public:
    ChangedStatus(QWidget *parent = 0);

private:
    QLabel* mChangedText;

public slots:
    void setChangedText(const QString& text);
};

}

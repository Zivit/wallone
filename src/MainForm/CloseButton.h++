#pragma once

#include <QtWidgets>


namespace wall
{

/*!
 * \class CloseButton
 * \brief Кнопка выхода из программы.
 */
//==============================================================================
class CloseButton : public QPushButton
{
public:
    CloseButton(QWidget* parent = 0);

};

}

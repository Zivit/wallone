#include "WallpaperButton.h++"


namespace wall
{

//------------------------------------------------------------------------------
WallpaperButton::WallpaperButton(QWidget *parent) : QPushButton(parent)
{
    //! \todo Подправить шрифт
    this->setStyleSheet("QPushButton {"
                        "	width: 200px;"
                        "	height: 50px;"
                        "	border: 1px solid #ffd409;"
                        "	color: white;"
                        "	font: bold 13px PTSans;"
                        "}"
                        ""
                        "QPushButton:hover {"
                        "	color: #ffd409;"
                        "}");

    this->setFocusPolicy(Qt::NoFocus);
}

}
